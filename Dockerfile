FROM httpd:2.4
LABEL website="peraprojects"
WORKDIR ./app/
COPY ./app/* /usr/local/apache2/htdocs/
