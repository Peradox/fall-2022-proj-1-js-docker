"use strict"
/**the first row of the table contains all the columns's names 
 * @author Minh
 */
 function createDefaultTable(){
    tableNode.setAttribute('id','table-node');
    let firstRow = document.createElement('tr');
    let pidCol = document.createElement("td");
    let nameCol= document.createElement("td");
    let titleCol =document.createElement("td");
    let categoryCol =document.createElement("td");
    let hoursCol =document.createElement("td");
    let rateCol= document.createElement("td");
    let statusCol = document.createElement('td');
    let descCol= document.createElement('td');
    let actionsCol= document.createElement('td');
    actionsCol.colSpan=2;
    actionsCol.setAttribute("width", "15%");
    pidCol.innerHTML="project_id";
    nameCol.innerHTML="owner_name";
    titleCol.innerHTML="project_title";
    categoryCol.innerHTML="project_category";
    hoursCol.innerHTML="nber_hours";
    rateCol.innerHTML="rate";
    statusCol.innerHTML="project_git status";
    descCol.innerHTML="description";
    actionsCol.innerHTML="Actions";
    firstRow.appendChild(pidCol);
    firstRow.appendChild(nameCol);
    firstRow.appendChild(titleCol);
    firstRow.appendChild(categoryCol);
    firstRow.appendChild(hoursCol);
    firstRow.appendChild(rateCol);
    firstRow.appendChild(statusCol);
    firstRow.appendChild(descCol);
    firstRow.appendChild(actionsCol);
    tableNode.appendChild(firstRow);
    document.querySelector("#table-render-space").replaceChildren(tableNode);

    for(let i=0;i<8;i++){
        let sortBtn=document.createElement('input');
        sortBtn.setAttribute("type","button");
        sortBtn.setAttribute("value", "Sort");
        sortBtn.setAttribute("class", "sortBtns");
        sortBtn.id=`S${i}`
        document.querySelectorAll("td")[i].appendChild(sortBtn);
    }
}
/**
 * replaces the values in the table with values from an array
 * @param {Array} array 
 * @author Pera
 */
 function createTableFromArray(array){
    tableNode.innerHTML="";
    createDefaultTable();
    let i = 1;
    array.forEach(function(obj){
        let newRow=document.createElement("tr");
        newRow.id="R"+i;
        let values=Object.values(obj);
        values.forEach(function(value){
            let newCell=newRow.appendChild(document.createElement("td"));
            newCell.innerHTML=value;
            newRow.appendChild(newCell);
            tableNode.appendChild(newRow);
        });
        let deleteCell = document.createElement('td');
        let delButton = document.createElement("input");
        
        delButton.setAttribute("type","button");
        delButton.setAttribute("value","delete");
        delButton.setAttribute("class", "delButtons");
        deleteCell.appendChild(delButton);
        delButton.id="D"+i;
        newRow.appendChild(deleteCell);
        
        

        let editCell = document.createElement("td");
        let editBtn = document.createElement("input");
        editBtn.setAttribute("type","button");
        editBtn.setAttribute("value","edit");
        editBtn.setAttribute("class","editBtn");
        editBtn.id="E"+i;
        editCell.appendChild(editBtn);
        newRow.appendChild(editCell);
        i++;
    });   
}

/**
 * A function for validating inputs from the page's form.
 * @returns {boolean}
 * @author Pera, Minh
 */
function isInputValid(){
    let pid = document.querySelector("#pid").value,
        name = document.querySelector("#name").value,
        title = document.querySelector("#title").value,
        category = document.querySelector("#category").value,
        hours = document.querySelector("#hour").value,
        rate = document.querySelector("#rate").value,
        status = document.querySelector('#status').value,
        desc = document.querySelector('#desc').value;
    
        let pidRegEx=/^[a-zA-Z][a-zA-Z0-9$\-_]{2,9}$/; //begins with a letter followed by 2-9 letters, numbers, $,- or _
        let nameRegEx=/^[a-zA-Z][a-zA-Z0-9\-]{2,9}$/; //begins with a letter followed by 2-9 letters, numbers or -
        let titleRegEx= /^\w{3,25}$/; //3-25 word characters
        let descRegEx= /^.{3,65}$/; //3-65 of any character
        let numbersRegEx= /^[0-9]{1,3}$/; //can only be between 1 and 3 digits
        let optionsRegEx= /[^-]/; //no dash

        let validInput=false,
            pidValid=pidRegEx.test(pid),
            nameValid=nameRegEx.test(name),
            titleValid=titleRegEx.test(title),
            descValid=descRegEx.test(desc),
            hoursValid=numbersRegEx.test(hours),
            rateValid=numbersRegEx.test(rate),
            catValid=optionsRegEx.test(category),
            statusValid=optionsRegEx.test(status);

        
            
            displayErrMessage(pidRegEx,pid,"pid-err");
            displayErrMessage(nameRegEx,name,"name-err");
            displayErrMessage(titleRegEx,title,"title-err");
            displayErrMessage(descRegEx,desc,"desc-err");
            displayErrMessage(numbersRegEx,hours,"hour-err");
            displayErrMessage(numbersRegEx,rate,"rate-err");
            displayErrMessage(optionsRegEx,status,"status-err");
            displayErrMessage(optionsRegEx,category,"cate-err");
           
            
        if(pidValid&&nameValid&&titleValid&&descValid&&hoursValid&&rateValid&&catValid&&statusValid){
            validInput=true;
        }
        return validInput;
    
}

/**
 * Displays a message whether the data is valid or not
 * @param {RegExp} regex 
 * @param {string} fieldId 
 * @param {string} errId 
 * @author Minh
 */
function displayErrMessage(regex,fieldId,errId){
    document.createElement("p");
    switch(regex.test(fieldId)){
        case false:
            document.querySelector(`#${errId}`).innerHTML="Invalid value❌";
            document.querySelector(`#${errId}`).style.color="red";
            document.querySelector(`#${errId}`).style.backgroundColor= "#FFCCCB";
            break;
        case true: 
            document.querySelector(`#${errId}`).innerHTML="✔";
            document.querySelector(`#${errId}`).style.color="green";
            document.querySelector(`#${errId}`).style.backgroundColor= "#90EE90";
            break;
    }
}