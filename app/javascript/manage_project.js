"use strict";

/**add an object to the array
 * @author Minh
 */
function addArray(){
    let pid = document.querySelector("#pid").value;
    let name = document.querySelector("#name").value;
    let title = document.querySelector("#title").value,
        category = document.querySelector("#category").value,
        hours = document.querySelector("#hour").value,
        rate = document.querySelector("#rate").value,
        status = document.querySelector('#status').value,
        description = document.querySelector('#desc').value;
    
    let project= {"pid":pid, "name":name, "proj_title":title, "proj_category":category, "proj_hrs":hours, "proj_rate":rate,"proj_status":status,"proj_desc":description};
    arr.push(project);

}
/**adds a row to the table based on last value of array of Projects
 * @author Minh
 */
function addRow(){
                
            let rowNode = document.createElement('tr');
            rowNode.id="R"+arr.length;
            let pidCell = document.createElement('td');
            let nameCell= document.createElement('td');
            let titleCell = document.createElement("td");
            let categoryCell = document.createElement("td");
            let hoursCell =document.createElement("td");
            let rateCell =document.createElement("td");
            let statusCell =document.createElement("td");
            let descriptionCell = document.createElement("td");

            pidCell.innerHTML=arr[arr.length-1]["pid"];
            nameCell.innerHTML=arr[arr.length-1]["name"];
            titleCell.innerHTML=arr[arr.length-1]["proj_title"];
            categoryCell.innerHTML=arr[arr.length-1]["proj_category"];
            hoursCell.innerHTML=arr[arr.length-1]["proj_hrs"];
            rateCell.innerHTML=arr[arr.length-1]["proj_rate"];
            statusCell.innerHTML=arr[arr.length-1]["proj_status"];
            descriptionCell.innerHTML=arr[arr.length-1]["proj_desc"];
            let deleteCell = document.createElement('td');
            let delButton = document.createElement("input");
            let editCell = document.createElement("td");
            let editBtn = document.createElement("input");
            delButton.setAttribute("type","button");
            delButton.setAttribute("value","delete");
            delButton.setAttribute("class", "delButtons");
            delButton.id="D"+arr.length;
            deleteCell.appendChild(delButton);
            editBtn.setAttribute("type","button");
            editBtn.setAttribute("value","edit");
            editBtn.setAttribute("class","editBtn");
            editBtn.id="E"+arr.length;
            editCell.appendChild(editBtn);

            rowNode.appendChild(pidCell);
            rowNode.appendChild(nameCell);
            rowNode.appendChild(titleCell);
            rowNode.appendChild(categoryCell);
            rowNode.appendChild(hoursCell);
            rowNode.appendChild(rateCell);
            rowNode.appendChild(statusCell);
            rowNode.appendChild(descriptionCell);
            rowNode.appendChild(deleteCell);
            rowNode.appendChild(editCell);
            document.querySelector("#table-node").appendChild(rowNode);

    
}

function editRow(event){
   let btnId = event.target.id;
   let index = btnId.slice(1);
   
    let Input0= document.createElement("input");
        Input0.setAttribute("type","text");
        Input0.value=arr[index-1]["pid"];
        let Input1= document.createElement("input");
        Input1.setAttribute("type","text");
        Input1.value=arr[index-1]["name"];
        let Input2= document.createElement("input");
        Input2.setAttribute("type","text");
        Input2.value=arr[index-1]["proj_title"];
        let Input3= document.createElement("input");
        Input3.setAttribute("type","text");
        Input3.value=arr[index-1]["proj_category"];
        let Input4= document.createElement("input");
        Input4.setAttribute("type","text");
        Input4.value=arr[index-1]["proj_status"];
        let Input5= document.createElement("input");
        Input5.setAttribute("type","text");
        Input5.value=arr[index-1]["proj_desc"];
   
   let nberInput1 = document.createElement("input");
   nberInput1.setAttribute("type","number");
   nberInput1.value=arr[index-1]["proj_hrs"];
   let nberInput2 = document.createElement("input");
   nberInput2.setAttribute("type","number");
   nberInput2.value=arr[index-1]["proj_rate"];
    
    let saveBtn = document.createElement("input");
    saveBtn.setAttribute("type","button");
    saveBtn.setAttribute("value","save");
    saveBtn.setAttribute("class","saveBtn");
    saveBtn.id="S"+index;
   
  
    document.querySelectorAll(`#R${index}> td`)[0].replaceChildren(Input0);
    document.querySelectorAll(`#R${index}> td`)[1].replaceChildren(Input1);
    document.querySelectorAll(`#R${index}> td`)[2].replaceChildren(Input2);
    document.querySelectorAll(`#R${index}> td`)[3].replaceChildren(Input3);
    document.querySelectorAll(`#R${index}> td`)[4].replaceChildren(nberInput1);
    document.querySelectorAll(`#R${index}> td`)[5].replaceChildren(nberInput2);
    document.querySelectorAll(`#R${index}> td`)[6].replaceChildren(Input4);
    document.querySelectorAll(`#R${index}> td`)[7].replaceChildren(Input5);
    document.querySelectorAll(`#R${index}> td`)[9].replaceChildren(saveBtn);

    saveBtn.addEventListener("click",function(){
        let allFieldsValidation=editRowValidation(document.querySelectorAll(`#R${index}> td > input`));
        let allFieldsValid=allFieldsValidation.every(field => field);
        if(allFieldsValid){
            arr[index-1]["pid"]=document.querySelectorAll(`#R${index}> td > input`)[0].value;
            arr[index-1]["name"]=document.querySelectorAll(`#R${index}> td >input`)[1].value;
            arr[index-1]["proj_title"]=document.querySelectorAll(`#R${index}> td>input`)[2].value;
            arr[index-1]["proj_category"]=document.querySelectorAll(`#R${index}> td>input`)[3].value;
            arr[index-1]["proj_hrs"]=document.querySelectorAll(`#R${index}> td>input`)[4].value;
            arr[index-1]["proj_rate"]=document.querySelectorAll(`#R${index}> td>input`)[5].value;
            arr[index-1]["proj_status"]=document.querySelectorAll(`#R${index}> td > input`)[6].value;
            arr[index-1]["proj_desc"]=document.querySelectorAll(`#R${index}> td > input`)[7].value;
            createTableFromArray(arr);
        }
    });
    
}
/**
 * validation for editing a row of input (text) fields. 
 * @param {NodeList} inputList 
 * @returns {boolean[]}
 * @author Pera
 */
function editRowValidation(inputList){
    let pidRegEx=/^[a-zA-Z][a-zA-Z0-9$\-_]{2,9}$/; //begins with a letter followed by 2-9 letters, numbers, $,- or _
    let nameRegEx=/^[a-zA-Z][a-zA-Z0-9\-]{2,9}$/; //begins with a letter followed by 2-9 letters, numbers or -
    let titleRegEx= /^\w{3,25}$/; //3-25 word characters
    let catRegex=/^pract$|^theory$|^fund_res$/; //only matches these three options
    let numbersRegEx= /^[0-9]{1,3}$/; //can only be between 1 and 3 digits
    let statusRegex=/^complete$|^planned$|^ongoing$/; //only matches these three options
    let descRegEx= /^.{3,65}$/; //3-65 of any character
    let regExArr=[pidRegEx, nameRegEx, titleRegEx, catRegex, numbersRegEx, numbersRegEx, statusRegex, descRegEx];
    let booleanArr=[];

    for(let i=0;i<regExArr.length;i++){
        let test=regExArr[i].test(inputList[i].value);
        booleanArr.push(test);
        if(test){
            inputList[i].style.backgroundColor= "#90EE90";
        }
        else{
            inputList[i].style.backgroundColor= "#FFCCCB";
        } 
    }
    return booleanArr;
}


/**empty all the input fields when the reset button is click
 * @author Minh
 */
function emptyInput(){
    function empty(e){
        e.value = null;
    }
    document.querySelectorAll(".emp").forEach(empty);
    document.querySelector("#category").value="-";
    document.querySelector("#status").value="-";
}
/**empty all the error message when the reset button is click
 * @author Minh
 */
function emptyMessage(){
    function empty(e){
       e.removeChild(e.firstChild);
    }
    document.querySelectorAll(".errMessage").forEach(empty);
}
/**delete a row when the delete button is clicked
 * @author Pera
 */
function deleteRow(index){
    arr.splice(index, 1);
    createTableFromArray(arr);
}

/**
 * Searches through array that holds the table's values and displays result.
 * @param {string} string 
 * @returns {number}
 * @author Pera
 */
function searchThroughTable(string){
    //matchingRows only serves to track how many rows a query returns.
    //the actual search function is not dependent on it.
    let matchingRows=0;
    for(let i=0;i<arr.length;i++){
        let values=Object.values(arr[i]);
        function checkTextContent(value){
            let found=value.includes(string);
            return found;
        }
        let found=values.find(checkTextContent);
        if(found){
            document.querySelectorAll("tr")[i+1].style= "display: table-row";
            matchingRows++;
        }
        else{
            document.querySelectorAll("tr")[i+1].style= "display: none";
        }
     }
    //if search is empty or has only spaces it goes back to normal/does not change
    if(string===""||/^\s+$/.test(string)){
        //matchingRows=-1 because this is not considered a query
        matchingRows=-1;
        createTableFromArray(arr);
    }
    return matchingRows;
}



/**
 * stores projects that currently exist in an array in string format
 * @author Pera
 */
function storeCurrentProjects(){
    let projArray=new Array();
    projArray=JSON.stringify(arr);
    localStorage.setItem("projectStorage",projArray);
}
/**
 * Returns an array of project objects stored in JSON string format 
 * @returns {Array}
 * @author Pera
 */
function getStoredProjects(){
    let storedProjs=localStorage.getItem("projectStorage");
    storedProjs=JSON.parse(storedProjs);
    return storedProjs;
}
/**
 * adds projects to local storage without affecting already stored projects.
 * @author Pera
 */
function appendProjects(){
    let storedProjs=getStoredProjects();
    let updatedProjs=storedProjs.concat(arr);
    updatedProjs=JSON.stringify(updatedProjs);
    localStorage.setItem("projectStorage",updatedProjs);
}

/**
 * shows the last executed operation.
 * @param {event} event 
 * @author Pera
 */
function showStatus(event){
    let status=document.querySelector("#status-bar");
    switch(event.target){
        case document.querySelector("#add"):
            status.textContent="Added a row";
            break;
        case document.querySelector("#searchbar"):
            if(searchThroughTable(event.target.value)===-1){
                status.textContent="";
            }
            else{
                status.textContent=`Query returned ${searchThroughTable(event.target.value)} project(s)`;
            }
            break;
        case document.querySelector("#save-local"):
            status.textContent=`Saved ${arr.length} project(s) to local storage.`;
            break;
        case document.querySelector("#load-local"):
            status.textContent=`Loaded ${arr.length} project(s) from local storage.`;
            break;
        case document.querySelector("#append-local"):
            status.textContent=`Added ${arr.length} project(s) to local storage.`;
            break;
        case document.querySelector("#clear-local"):
            status.textContent="Cleared local storage."
            break;
        default:
            if(event.target.classList.contains("delButtons")){
                status.textContent="Deleted a row."
            }
            else{
                status.textContent="";
            }
            break;
    }
}
/**
 * Given a string corresponding to an object key, sorts
 * both the array of projects and table of projects based on that field.
 * @param {Event} event
 * @author Pera
 */
function sortProjects(event){
    let btnId = event.target.id;
    let index = btnId.slice(1);
    let fieldKey= Object.keys(arr[1])[index];
    let fieldArr=arr.map((project)=>{return project[fieldKey]});

    if(fieldKey==="hours"||fieldKey==="rate"){
        fieldArr.sort((a,b)=>{a-b});
    }
    else{
        fieldArr.sort();
    }

    arr.sort((a, b)=>{
        return fieldArr.indexOf(a[fieldKey]) - fieldArr.indexOf(b[fieldKey]);
    });

    createTableFromArray(arr);
}


