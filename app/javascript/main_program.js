"use strict"
let tableNode = document.createElement('table');
let arr = new Array();
let addBtn =document.querySelector("#add");
let resetBtn= document.querySelector("#reset");
let saveBtn=document.querySelector("#save-local");
let loadBtn=document.querySelector("#load-local");
let appendBtn=document.querySelector("#append-local");
let clearBtn=document.querySelector("#clear-local");

addEventListener("DOMContentLoaded", createDefaultTable);

addBtn.disabled=true;
saveBtn.disabled=true;
if(localStorage.length===0){
    appendBtn.disabled=true;
}
//checks if all inputs are valid every time a value is changed
for(let i=0;i<document.querySelectorAll("#main-section > fieldset > *").length;i++){
    document.querySelectorAll("#main-section > fieldset > *")[i].addEventListener("change", (event)=>{
        if(isInputValid()){
            addBtn.disabled=false;
            addBtn.addEventListener("click",addArray);
            addBtn.addEventListener("click",addRow);
            addBtn.addEventListener("click", showStatus);
            addBtn.addEventListener("click", (e)=>saveBtn.disabled=false);
        }
        else{
            addBtn.disabled=true;
        }
    });
}


resetBtn.addEventListener("click",emptyInput);
resetBtn.addEventListener("click",emptyMessage);
resetBtn.addEventListener("click", (event) => {addBtn.disabled=true;});
resetBtn.addEventListener("click", showStatus);



saveBtn.addEventListener("click",storeCurrentProjects);
saveBtn.addEventListener("click",showStatus);
saveBtn.addEventListener("click", (e)=> appendBtn.disabled=false);

loadBtn.addEventListener("click", function(e){
    if(confirm(`The items you load will overwrite the ones in the current table. 
    Select Cancel if you would like to go back and append current projects to local storage.`)){
        let savedProjs=getStoredProjects();
        if(savedProjs!=null){
            arr=savedProjs;
            createTableFromArray(arr);
            saveBtn.disabled=false;
        }
        showStatus(e);
    }
});

appendBtn.addEventListener("click", appendProjects);
appendBtn.addEventListener("click", showStatus);

clearBtn.addEventListener("click", function(e){
    if(confirm("This will delete your projects in local storage. Are you sure?")){
        localStorage.clear();
        showStatus(e);
    }
});


document.querySelector("#searchbar").addEventListener("blur", function(e){
    searchThroughTable(document.querySelector("#searchbar").value);
});
document.querySelector("#searchbar").addEventListener("blur",showStatus);

document.addEventListener("click", (event)=>{
    if(event.target && event.target.classList.contains("sortBtns")){
        sortProjects(event);
    }
});

//adds an event listener to the document, but the event only activates when triggered by elements
//from the delButtons class. When a delete button is pressed it deletes the respective row
document.addEventListener("click", function(e){
    
    if(e.target && e.target.classList.contains("delButtons")){
        if(confirm("Are you sure you want to delete this row?")){
            let delBtns=document.getElementsByClassName("delButtons");
            for(let i=0;i<arr.length;i++){
                if(delBtns[i]===e.target){
                    
                    deleteRow(i);
                    
                }
            }
            showStatus(e);
            if(arr.length===0){
                saveBtn.disabled=true;
            }
        }
    }
});

document.addEventListener("click", function(e){
    
    if(e.target && e.target.classList.contains("editBtn")){
        editRow(e);
        
        
            
        
    }
})